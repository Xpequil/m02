# E2. Exercici 1. Comandes i Operadors

## Introducció

## Continguts

Amb els fiters i directoris de la carpeta exercici responeu les segûents preguntes:

Indiqueu en cada pregunta l'ordre i també el resultat obtingut (podeu copiar del terminal)

## Entrega

1. ** Feu una búsqueda que us retorni els fitxers de la carpeta exercici (i només aquesta) que comecin per 'pro'**.
	- Ordre:
	- Sortida:

2. ** Feu una búsqueda que us retorni els fitxers de totes les carpetes que comecin per 'pro'**.
	- Ordre:
	- Sortida:
	
3. ** Feu una búsqueda que us retorni els fitxers de la carpeta exercici (i només aquesta) que comencin per 'pr' i tinguin extensió 'txt'**.
	- Ordre:
	- Sortida:

4. ** Feu una búsqueda que us retorni els fitxers de totes les carpetes que comencin per 'pr', tinguin al tercer caràcter una 'i' o una 'o' i acabin amb 'va.txt'**.
	- Ordre:
	- Sortida:

5. ** Feu una búsqueda que us retorni els fitxers de totes les carpetes que continguin 'prova.csv'**.
	- Ordre:
	- Sortida:
	
6. ** Feu una búsqueda que us retorni els fitxers de totes les carpetes que acabin en 've.txt'**.
	- Ordre:
	- Sortida:
	
7. ** Busqueu tots els fitxers que tenen la paraula 'freedom' (només en minúscules) en el seu contingut.**
	- Ordre:
	- Sortida:

8. ** Busqueu tots els fitxers que tenen la paraula 'freedom' (minúscules o majúscules) en el seu contingut.**
	- Ordre:
	- Sortida:

9. ** Envieu el contingut de tots els fitxers del directori 'quotes' a un fitxer dins de la carpeta exercicis anomenat 'quotes.mix'**
	- Ordre:

10. ** Què farà l'ordre següent? No l'executeu, només heu d'explicar què farà pas per pas.** 
	- Ordre: echo "Millor no ho provis" && sleep 5 && echo "Encara hi ets a temps!" && sleep 5 && touch janohietsatemps && shutdown -h now
	- Explicació:
